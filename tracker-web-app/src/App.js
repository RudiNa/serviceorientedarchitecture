import React, { Component } from 'react';
import './App.css';
import Navigation from './components/router';

class App extends Component {
  render() {
    return (
      <div >
        {/* <header className="App-header"> */}
        <Navigation></Navigation>
        {/* </header> */}
      </div >
    );
  }
}

export default App;
