const BASE_URL = "http://localhost:50605/api/";
// const BASE_URL_s = "https://localhost:44384/api/";

const WORKDAY_ENTRY = "workday";
const WORK_ORDER = "workorder";

const ADD_REQUEST = "add";
const GET_REQUEST = "get";
const UPDATE_REQUEST = "update";
const DELETE_REQUEST = "delete";

class API {
    static getWorkDay() {
        return fetch(BASE_URL + WORKDAY_ENTRY)
            .then(r => r.json());
    };

    static updateWorkDayEntry() {
        return fetch(BASE_URL + WORKDAY_ENTRY + UPDATE_REQUEST);
    }

    static addWorkDayEntry(time, workOrder, workEntry, description) {
        return fetch(BASE_URL + WORKDAY_ENTRY, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                workedTime: time,
                workOrder: workOrder,
                entry: workEntry,
                description: description
            })
        }).then(respone => respone);
    }

    static deleteWorkDayEntry(id) {
        return fetch(BASE_URL + WORKDAY_ENTRY + "?id=" + id, {
            method: 'DELETE'
        }).then(response => response);
    };

    static getWorkOrder() {
        return fetch(BASE_URL + WORK_ORDER)
            .then(result => result.json());
    }

    static addWorkOrder(title, description) {
        return fetch(BASE_URL + WORK_ORDER, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                title: title,
                isJiraIntegration: 'true',
                description: description
            })
        }).then(respone => respone);
    }

    static deleteWorkOrder(id) {
        return fetch(BASE_URL + WORK_ORDER + "?id=" + id, {
            method: 'DELETE'
        }).then(response => response);
    };

    static getSettings() {
        return fetch(BASE_URL + WORKDAY_ENTRY);
    }
}

const checkStatus = ((response) => {
    if (response.ok) {
        return response;
    } else {
        var error = new Error(response.statusText);
        console.log(error);
        //return Promise.reject(error);
    }
})

export default API;