import React, { Component } from 'react'
import Panel from 'react-bootstrap/lib/Panel';
import PanelGroup from 'react-bootstrap/lib/PanelGroup';
import Api from '../common/api'
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';

class WorkDayEntry extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            workDay: [],
        }
    }

    componentDidMount() {
        Api.getWorkDay().then(work => {
            this.setState({ workDay: work });
        });
    }

    deleteWorkDayEntry = entryId => {
        Api.deleteWorkDayEntry(entryId);
        // #todo update in real time 
        Api.getWorkDay().then(work => {
            this.setState({ workDay: work });
        });
    }

    render() {
        return (
            <div className="App-panel">
                <PanelGroup accordion id="accordion-example">
                    {
                        this.state.workDay.map((p, index) => {
                            return <Panel eventKey={index} key={p.id}>
                                <form>

                                    <Panel.Heading>
                                        <Panel.Title toggle>
                                            <Col md={8}>{p.workOrder} - {p.entry} </Col>
                                            <Col xsOffset={11} >
                                                <Button type="submit"
                                                    onClick={() => { this.deleteWorkDayEntry(p.id) }}
                                                >x
                                                    </Button>
                                            </Col>
                                        </Panel.Title>
                                    </Panel.Heading>
                                    <Panel.Body collapsible>
                                        {p.description}
                                    </Panel.Body>
                                </form>

                            </Panel>
                        })
                    }
                </PanelGroup>
            </div>
        );
    }
}

export default WorkDayEntry;