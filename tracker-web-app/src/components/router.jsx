import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Menu from './menu';
import WorkDay from './work-day';
import WorkOrder from './work-order';

class Navigation extends Component {
    render() {
        return (
            <Router>
                <div>
                    <Menu></Menu>
                    <Route path="/work-day" component={WorkDay}></Route>
                    {/* <Route path="/setting" component={}></Route>
                    <Route path="/report" component={}></Route>
                    <Route path="/stats" component={}></Route>*/}
                    <Route path="/work-order" component={WorkOrder}></Route>
                </div>
            </Router>
        );
    }
}

export default Navigation;