import React, { Component } from 'react';
import Nav from 'react-bootstrap/lib/Nav'
import Navbar from 'react-bootstrap/lib/Navbar'
import NavItem from 'react-bootstrap/lib/NavItem'

import api from '../common/api'
import clock from '../clock.svg';
import pointer from '../pointer.svg';

class Menu extends Component {
    getWorkDay() {
        return api.getWorkDay();
    }

    render() {
        return (
            <Navbar>
                <Navbar.Header>
                    <Navbar.Brand>
                        <img src={clock} className="App-clock App-logo" alt="clock" />
                        <img src={pointer} className="App-pointer App-logo" alt="clock" />
                    </Navbar.Brand>
                </Navbar.Header>
                <Nav >
                    <NavItem eventKey={1} href="/work-day" to="/work-day">Work Day
                    </NavItem>
                    <NavItem eventKey={2} href="/setting">Settings
                    </NavItem>
                    <NavItem eventKey={3} href="#">Reports
                    </NavItem>
                    <NavItem eventKey={4} href="#">Stats
                    </NavItem>
                    <NavItem eventKey={5} href="/work-order" to="/work-order">Work Orders
                    </NavItem>
                    <NavItem eventKey={6} href="#">Timer
                    </NavItem>
                    <NavItem eventKey={7} >Working Month
                    </NavItem>
                </Nav>
            </Navbar>
        );
    }
}

export default Menu;