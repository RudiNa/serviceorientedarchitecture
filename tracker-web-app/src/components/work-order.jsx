import React, { Component } from 'react'
import Button from 'react-bootstrap/lib/Button';
import Col from 'react-bootstrap/lib/Col';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import FormControl from 'react-bootstrap/lib/FormControl';
import Checkbox from 'react-bootstrap/lib/Checkbox';
import WorkOrderList from './work-order-list';
import Api from '../common/api';

class WorkOrder extends Component {
    constructor() {
        super();

        this.state = {
            title: '',
            description: ''
        }
    }

    componentDidMount() {

    }

    handleTitleChange(e) {
        this.setState({ title: e.target.value });
    }

    handleDescriptionChange(e) {
        this.setState({ description: e.target.value });
    }

    addWorkOrder(title, description) {
        Api.addWorkOrder(title, description);
    }

    render() {
        return (
            <div>
                <Col md={6} >
                    <form>
                        <FormGroup
                            controlId="formTime"
                        // validationState={this.getTimeValidationState()}
                        >
                            <FormControl
                                type="text"
                                value={this.state.title}
                                placeholder="Title"
                                onChange={this.handleTitleChange}
                            />
                        </FormGroup>
                        <Checkbox>
                            Jira Integration
    </Checkbox>
                        <FormGroup
                            controlId="formDescription"
                        // validationState={this.getDescriptionValidationState()}
                        >
                            <FormControl
                                componentClass="textarea"
                                value={this.state.description}
                                placeholder="Short review."
                                onChange={this.handleDescriptionChange}
                            />
                            <FormControl.Feedback />
                        </FormGroup>
                        <Button type="submit"
                            onClick={() => this.addWorkOrder(this.state.title, this.state.description)}>Save</Button>

                    </form>
                </Col>
                <Col md={6} style={{ alignItems: 'center' }}>
                    <WorkOrderList></WorkOrderList>
                </Col>
            </div>
        );
    }
}

export default WorkOrder