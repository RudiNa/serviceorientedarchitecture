import React, { Component } from 'react';
import Button from 'react-bootstrap/lib/Button';
import Col from 'react-bootstrap/lib/Col';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import FormControl from 'react-bootstrap/lib/FormControl';

import { PieChart, Pie } from 'recharts';

import Api from '../common/api'
import WorkDayEntry from './work-day-entry';

const data01 = [{ name: 'Group A', value: 400 }, { name: 'Group B', value: 300 },
{ name: 'Group C', value: 300 }, { name: 'Group D', value: 200 }]

class WorkDay extends Component {

    constructor(props, context) {
        super(props, context);
        this.handleTimeChange = this.handleTimeChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);

        this.state = {
            time: '',
            dayOfWeek: this.getDayOfWeek(),
            description: '',
            externalChart: [{ name: 'Unworked Time', value: 24, fill: "#808080" },
            { name: 'Worked Time', value: 0, fill: "#99ff99" },],
            workDay: [],
            entry: '',
            workOrder: ''
        };
    }

    componentDidMount() {
        var work = Api.getWorkDay().then(work => {
            this.setState({ workDay: work.data });
            return work;
        })
        var workedHours = 0;

        work.then(function (result) {
            result.map((workEntry, i) => {
                var h = workEntry.workedTime.indexOf("h");
                var m = workEntry.workedTime.indexOf("m");
                var hour = workEntry.workedTime.substring(0, h);
                var minute = workEntry.workedTime.substring(h + 1, m);
                workedHours = workedHours +
                    (h !== -1 ? parseInt(hour) : 0) +
                    (m !== -1 ? parseInt(minute) : 0) / 60;
                return null;
            });
        });
        setTimeout(() => {
            this.setState({
                externalChart: [{ name: 'Unworked Time', value: 24 - workedHours, fill: "#808080" },
                { name: 'Worked Time', value: workedHours, fill: "#99ff99" }]
            });
        }, 1000);
    }

    getDayOfWeek() {
        var d = new Date();
        var weekday = new Array(7);
        weekday[0] = "Sunday";
        weekday[1] = "Monday";
        weekday[2] = "Tuesday";
        weekday[3] = "Wednesday";
        weekday[4] = "Thursday";
        weekday[5] = "Friday";
        weekday[6] = "Saturday";

        return weekday[d.getDay()];
    }

    getTimeValidationState() {
        if (this.state.time === undefined || this.state.time === '')
            return null;
        let workedTime = this.state.time.split(" ");
        if (workedTime.length < 1 || workedTime.length > 2)
            return 'error';
        if (workedTime.length === 2) {
            if (workedTime[0].includes("h") && workedTime[1].includes("m"))
                return 'success';
        } else {
            if (workedTime[0].includes("h") || workedTime[0].includes("m"))
                return 'success';
        }
        return 'error';
    }

    addWorkDayEntry(time, workOrder, entry, description) {
        Api.addWorkDayEntry(time, workOrder, entry, description);
    }

    getDescriptionValidationState() {
        if (this.state.description === undefined || this.state.description === '')
            return null;
        if (this.state.description.length !== 0)
            return 'success';
        return 'error';
    }

    handleTimeChange(e) {
        this.setState({ time: e.target.value });
    }
    handleDescriptionChange(e) {
        this.setState({ description: e.target.value });
    }

    onWorkEntrySelected(v) {
        this.setState({ entry: this.inputEle.value });
    }

    onWorkOrderSelected(v) {
        this.setState({ workOrder: this.inputEl.value });
    }
    render() {
        return (
            <div>
                <Col md={6} >
                    <h4>DUDe - {this.state.dayOfWeek}</h4>

                    <form>
                        <FormGroup
                            controlId="formTime"
                            validationState={this.getTimeValidationState()}
                        >
                            <FormControl
                                type="text"
                                value={this.state.time}
                                placeholder="Worked Time. Format: 'x'h 'y'm"
                                onChange={this.handleTimeChange}
                            />
                        </FormGroup>
                        <FormGroup
                            controlId="formWorkDay"
                        // validationState={this.getWorkDayValidationState()}
                        >
                            <FormControl componentClass="select"
                                onChange={this.onWorkOrderSelected.bind(this)}
                                inputRef={el => this.inputEl = el}
                            >
                                <option selected disabled>Work Order</option>
                                <option>Action</option>
                                <option >Order</option>
                            </FormControl>
                        </FormGroup>
                        <FormGroup
                            controlId="formEntry"
                        // validationState={this.getEntryValidationState()}
                        >
                            <FormControl componentClass="select"
                                inputRef={el => this.inputEle = el}
                                onChange={this.onWorkEntrySelected.bind(this)}
                            >
                                <option selected disabled>Entry</option>
                                <option >Action</option>
                                <option >Another action</option>
                            </FormControl>
                        </FormGroup>
                        <FormGroup
                            controlId="formDescription"
                            validationState={this.getDescriptionValidationState()}
                        >
                            <FormControl
                                componentClass="textarea"
                                value={this.state.description}
                                placeholder="Short review."
                                onChange={this.handleDescriptionChange}
                            />
                            <FormControl.Feedback />
                        </FormGroup>
                        <Button //type="submit"
                            onClick={() => this.addWorkDayEntry(this.state.time, this.state.workOrder, this.state.entry, this.state.description)}>Save</Button>

                    </form>
                    <PieChart width={800} height={400}>
                        <Pie dataKey="value" data={data01} cx={200} cy={200} outerRadius={60} fill="#cc99ff" />
                        <Pie dataKey="value" data={this.state.externalChart} cx={200} cy={200} innerRadius={70} outerRadius={90} label />
                    </PieChart>

                </Col>
                <Col md={6} style={{ alignItems: 'center' }}>
                    <WorkDayEntry></WorkDayEntry>
                </Col>
            </div>
        );
    }
}

export default WorkDay;