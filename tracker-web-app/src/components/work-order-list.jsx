import React, { Component } from 'react'
import Panel from 'react-bootstrap/lib/Panel';
import PanelGroup from 'react-bootstrap/lib/PanelGroup';
import Api from '../common/api'
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';

class WorkOrderList extends Component {
    constructor() {
        super();

        this.state = {
            workOrder: []
        }
    }

    componentDidMount() {
        Api.getWorkOrder().then(work => {
            this.setState({ workOrder: work });
        });
    }

    deleteWorkOrder(id) {
        Api.deleteWorkOrder(id);
    }

    render() {
        return (
            <div className="App-panel">
                <PanelGroup accordion id="accordion-example">
                    {
                        this.state.workOrder.map((p, index) => {
                            return <Panel eventKey={index}>
                                <form>

                                    <Panel.Heading>
                                        <Panel.Title toggle>
                                            <Col md={8}>{p.name} </Col>
                                            <Col xsOffset={11} >
                                                <Button type="submit"
                                                    onClick={() => { this.deleteWorkOrder(p.id) }}
                                                >x
                                                    </Button>
                                            </Col>
                                        </Panel.Title>
                                    </Panel.Heading>
                                    <Panel.Body collapsible>
                                        {p.description}
                                    </Panel.Body>
                                </form>

                            </Panel>
                        })
                    }
                </PanelGroup>
            </div>
        );
    }
}
export default WorkOrderList;