﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Repository
{
    public class FreeDayRepository
    {
        private const string URL = "http://localhost:50253/";
        private string urlParameters = "values/getfreeday?";

        public bool GetFreeDay(DateTime today, int userId)
        {
            urlParameters += "userId=" + userId + "&date=" + today.ToString();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body.
                var responseString = response.Content.ReadAsStringAsync().Result;
                var isFreeDay = false;
                bool.TryParse(responseString, out isFreeDay);
                return isFreeDay;
            }
            //Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
            client.Dispose();
            return false;
        }
    }

    public class FreeDay
    {

    }
}
