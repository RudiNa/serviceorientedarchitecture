﻿using Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository
{
    public class WorkorderRepository
    {
        private List<WorkOrder> workOrders = new List<WorkOrder>()
            {
                new WorkOrder()
                {
                    Id = 1,
                    Description = "I Worked on something. What is your opinion?",
                    Title = "01 - WorkDay Form"
                },
                new WorkOrder()
                {
                    Id =2,
                    Description = "I Worked on something. What is your opinion?",
                    Title = "You",
                },
                new WorkOrder()
                {
                    Id= 3,
                    Description = "I Worked on something. What is your opinion?",
                    Title = "03 - WorkDay List"
                }
            };

        public List<WorkOrder> Get()
        {
            return workOrders;
        }

        public bool Insert(WorkOrder workOrder)
        {
            workOrder.Id = workOrders.Last().Id + 1;
            workOrders.Add(workOrder);
            return workOrders.Contains(workOrder);
        }

        public string Update(WorkDay workDayEntry)
        {
            return "adupdated";
        }

        public bool Remove(int id)
        {
            return workOrders.Remove(workOrders.First(x => x.Id == id));
        }
    }
}
