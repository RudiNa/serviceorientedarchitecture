﻿using Common.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Repository
{
    public class WorkdayRepository
    {
        private List<WorkDay> workDays = new List<WorkDay>()
            {
                new WorkDay()
                {
                    Id = 1,
                    Description = "I Worked on something. What is your opinion?",
                    WorkedTime = "4h",
                    WorkOrder = "You",
                    Entry = "01 - WorkDay Form"
                },
                new WorkDay()
                {
                    Id =2,
                    Description = "I Worked on something. What is your opinion?",
                    WorkedTime = "1h 30m",
                    WorkOrder = "You",
                    Entry = "02 - WorkDay Chart"
                },
                new WorkDay()
                {
                    Id= 3,
                    Description = "I Worked on something. What is your opinion?",
                    WorkedTime = "2h 15m",
                    WorkOrder = "You",
                    Entry = "03 - WorkDay List"
                }
            };

        public List<WorkDay> Get()
        {
            return workDays;
        }

        public bool Update(WorkDay workDay)
        {
            if(!workDays.Exists(x=>x.Id == workDay.Id)) return false;
            // to do update list
            return true;
        }

        public bool Insert(WorkDay workDay)
        {
            workDay.Id = workDays.Last().Id + 1;
            workDays.Add(workDay);
            return workDays.Contains(workDay);
        }

        public bool Delete(int id)
        {
            return workDays.Remove(workDays.First(x => x.Id == id));
        }
    }
}
