﻿using Common.Model;
using Microsoft.AspNetCore.Mvc;

namespace Tracker_App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SettingsController : ControllerBase
    {
        public Setting setting = new Setting()
        {
            Notification = true
        };

        [HttpGet]
        public ActionResult<Setting> Get()
        {
            return Ok(setting);
        }

        [HttpPost]
        public ActionResult<string> Add()
        {
            return "add";
        }
    }
}