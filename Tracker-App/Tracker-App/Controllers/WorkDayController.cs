﻿using Common.Model;
using Microsoft.AspNetCore.Mvc;
using Service;

namespace Tracker_App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorkDayController : ControllerBase
    {
        private WorkdayService _workdayService = new WorkdayService();
        [HttpGet]
        public ActionResult<string> Get()
        {
            var wordays = _workdayService.GetWorkdays();
            return Ok(wordays);
        }

        [HttpPost]
        public ActionResult<bool> Add(WorkDay workDayEntry)
        {
            var result = _workdayService.AddWorkday(workDayEntry);
            return Ok(result);
        }

        [HttpPut]
        public ActionResult<string> Update(WorkDay workDayEntry)
        {
            var result = _workdayService.UpdateWorday(workDayEntry);
            return Ok(result);
        }

        [HttpDelete]
        public ActionResult<bool> Delete(int id)
        {
            var result = _workdayService.RemoveWorkday(id);
            return Ok(result);
        }
    }
}