﻿using Microsoft.AspNetCore.Mvc;
using Service;

namespace Tracker_App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FreeDayController : ControllerBase
    {
        private FreeDayService _freeDayService = new FreeDayService();

        public ActionResult<bool> GetFreeDay()
        {
            var userId = 10;
            return Ok(_freeDayService.GetFreeDay(userId));
        }
    }
}