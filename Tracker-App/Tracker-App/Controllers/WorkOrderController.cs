﻿using System.Collections.Generic;
using System.Linq;
using Common.Model;
using Microsoft.AspNetCore.Mvc;
using Service;

namespace Tracker_App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorkOrderController : ControllerBase
    {
        private WorkorderService _workOrderController = new WorkorderService();

        [HttpGet]
        public ActionResult<string> Get()
        {
            var workorders = _workOrderController.GetWorkorder();
            return Ok(workorders);
        }

        [HttpPost]
        public ActionResult<bool> Add(WorkOrder workOrder)
        {
            var result = _workOrderController.AddWorkorder(workOrder);
            return Ok(result);
        }

        [HttpPut]
        public ActionResult<string> Update(WorkDay workDayEntry)
        {
            return Ok(_workOrderController.UpdateWorkorer(workDayEntry));
        }

        [HttpDelete]
        public ActionResult<bool> Delete(int id)
        {
            return Ok(id);
        }
    }
}