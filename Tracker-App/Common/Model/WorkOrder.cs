﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Model
{
    public class WorkOrder
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsJiraIntegration { get; set; }
        public int Id { get; set; }
    }
}
