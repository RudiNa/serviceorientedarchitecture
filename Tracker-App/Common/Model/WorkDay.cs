﻿
namespace Common.Model
{
    public class WorkDay
    {
        public string WorkedTime { get; set; }
        public string Description { get; set; }
        public string WorkOrder { get; set; }
        public string Entry { get; set; }
        public int Id { get; set; }
    }
}
