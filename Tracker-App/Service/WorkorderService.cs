﻿using Common.Model;
using Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Service
{
    public class WorkorderService
    {
        private WorkorderRepository _workorderRepository = new WorkorderRepository();


        public List<WorkOrder> GetWorkorder()
        {
            return _workorderRepository.Get();
        }

        public bool AddWorkorder(WorkOrder workOrder)
        {
            return _workorderRepository.Insert(workOrder);
        }

        public string UpdateWorkorer(WorkDay workDayEntry)
        {
            return _workorderRepository.Update(workDayEntry);
        }

        public bool RemoveWorkorder(int id)
        {
            return _workorderRepository.Remove(id);
        }
    }
}
