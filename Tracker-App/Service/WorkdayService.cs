﻿using Common.Model;
using Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Service
{
    public class WorkdayService
    {
        private readonly WorkdayRepository _workdayRepository = new WorkdayRepository();

        public List<WorkDay> GetWorkdays()
        {
            return _workdayRepository.Get();
        }

        public bool UpdateWorday(WorkDay workDay)
        {
            return _workdayRepository.Update(workDay);
        }

        public bool AddWorkday(WorkDay workDay)
        {
            return _workdayRepository.Insert(workDay);
        }

        public bool RemoveWorkday(int id)
        {
            return _workdayRepository.Delete(id);
        }
    }
}
