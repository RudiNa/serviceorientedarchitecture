﻿using Repository;
using System;

namespace Service
{
    public class FreeDayService
    {
        private FreeDayRepository _freeDayRepository = new FreeDayRepository();

        public bool GetFreeDay(int userId)
        {
            DateTime today = DateTime.Now;
            return _freeDayRepository.GetFreeDay(today, userId);
        }
    }
}
