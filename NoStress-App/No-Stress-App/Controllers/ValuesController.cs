﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace No_Stress_App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private Dictionary<int, DateTime> freeDays = new Dictionary<int, DateTime>
        {
            { 1, DateTime.Now },
            { 2, DateTime.Now.AddDays(1) },
            { 3, DateTime.Now.AddDays(1) },
            { 4, DateTime.Now.AddDays(2) },
        };       

        // GET api/values
        [HttpGet]
        public ActionResult<bool> GetFreeDay(int id, DateTime time)
        {
            return Ok(freeDays.First(x=>x.Key == id && time.Day == x.Value.Day && time.Month == x.Value.Month && time.Year == x.Value.Year));
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
